package net.qqxh.sunflow.server.office.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.qqxh.sunflow.server.office.bean.OfficeFile;

/**
 * Copyright (C), 2019-2020, sunflow开发团队
 * 在线编辑文件 服务类
 *
 * @fileName OfficeFileService.java
 * @date     2019/5/25 15:37
 * @author   cjy
 */
public interface OfficeFileService extends IService<OfficeFile> {
}
